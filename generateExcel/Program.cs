﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generateExcel
{
    class Program
    {
        public static int categoryType = 0;

        static void Main(string[] args)

        {
            //Start Excel and get Application object.
            var oXL = new Microsoft.Office.Interop.Excel.Application();
            oXL.Visible = true;

            //Get a new workbook.
            var oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
            var oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

            //Add table headers going cell by cell.
            oSheet.Cells[1, 1] = "Age";
            oSheet.Cells[1, 2] = "Gender";
            oSheet.Cells[1, 3] = "PeopleCount";
            oSheet.Cells[1, 4] = "MinutesFromMidnight";
            oSheet.Cells[1, 5] = "LocationType";
            oSheet.Cells[1, 6] = "FamilyStatus";
            oSheet.Cells[1, 7] = "XCoordinate";
            oSheet.Cells[1, 8] = "YCoordinate";
            oSheet.Cells[1, 9] = "DayOfWeek";
            oSheet.Cells[1, 10] = "Label";

            //Format A1:D1 as bold, vertical alignment = center.
            oSheet.get_Range("A1", "J1").Font.Bold = true;
            oSheet.get_Range("A1", "J1").VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

            Console.WriteLine("How many docs?");
            int nDocsCount = int.Parse(Console.ReadLine());


            Console.WriteLine("Restaurent - 1, Pub - 2, Coffee - 3");
            categoryType = int.Parse(Console.ReadLine());

            // Create an array to multiple values at once.
            string[,] data = new string[nDocsCount, 10];

            for (int i = 0; i < nDocsCount; i++)
            {
                data[i, 0] = getAge().ToString();
                data[i, 1] = getGender();
                data[i, 2] = getPeopleCount().ToString();
                data[i, 3] = getMinutesFromMidnight().ToString();
                data[i, 4] = getPlaceCategory();
                data[i, 5] = getFamilyStatus();
                data[i, 6] = getXCoords().ToString();
                data[i, 7] = getYCoords().ToString();
                data[i, 8] = getDayOfWeek().ToString();
                data[i, 9] = getPlaceName();
            }

            oSheet.get_Range("A2", "J" + nDocsCount).Value2 = data;

            ////Fill C2:C6 with a relative formula (=A2 & " " & B2).
            //var oRng = oSheet.get_Range("C2", "C6");
            //oRng.Formula = "=A2 & \" \" & B2";

            ////Fill D2:D6 with a formula(=RAND()*100000) and apply format.
            //oRng = oSheet.get_Range("D2", "D6");
            //oRng.Formula = "=RAND()*100000";
            //oRng.NumberFormat = "$0.00";

            ////AutoFit columns A:D.
            //oRng = oSheet.get_Range("A1", "D1");
            //oRng.EntireColumn.AutoFit();

            oXL.Visible = false;
            oXL.UserControl = false;
            oWB.SaveAs(@"C:\Tom\colman\data.xlsx", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            oWB.Close();
            Process.Start(@"C:\Tom\colman\data.xlsx");
            //File.Open(@"C:\Tom\colman\data.xslx", FileMode.Open);
        }

        private static string getPlaceName()
        {
            if (categoryType == 1)
            {
                return getRestaurent();
            }
            else if (categoryType == 2)
            {
                return getPub();
            }
            else if (categoryType == 3)
            {
                return getCoffee();
            }
            return "None";
        }

        public static Random rndPlace = new Random();

        public static List<String> restaurents = new List<string>()
        { "Onza", "Bistro Bar", "Kitchen Market",
            "Taizo", "Minna Tomei", "Kimmel" , 
            "Uri Buri" , "Rak Basar", "Moon"
            , "Nono"
        };

        public static String getRestaurent()
        {
            return restaurents[rndPlace.Next(0, restaurents.Count)];
        }

        public static List<String> pubs = new List<string>()
        { "Hamishbezet", "Illka", "Panda Bar",
            "Becki", "Hapitria", "Hagnes" ,
            "Sheinkin" , "Twenty two", "Hasfsal"
            , "Jems"
        };

        public static String getPub()
        {
            return pubs[rndPlace.Next(0, pubs.Count)];
        }

        public static List<String> coffees = new List<string>()
        { "Aroma", "Cafe Cafe", "Landver",
            "Halma", "Hillel", "Jo" ,
            "Matilda" , "Montifiory", "Boka"
            , "La Mulan"
        };

        public static String getCoffee()
        {
            return coffees[rndPlace.Next(0, coffees.Count)];
        }



        private static string getPlaceCategory()
        {
            if (categoryType == 1)
            {
                return "Restaurent";
            }
            else if (categoryType == 2)
            {
                return "Pub";
            }
            else if (categoryType == 3)
            {
                return "Coffee";
            }
            return "None";
        }

        static Random ageRadnom = new Random();

        public static int getAge()
        {
            return ageRadnom.Next(15, 100);
        }

        static Random genderRadnom = new Random();

        public static string getGender()
        {
            int gender = genderRadnom.Next(0, 2);

            if (gender == 0)
            {
                return "M";
            }
            return "F";
        }

        static Random peopleCountRadnom = new Random();

        public static int getPeopleCount()
        {
            int countRand = peopleCountRadnom.Next(0, 100);

            if (countRand < 8)
            {
                return 1;
            }
            else if (countRand < 25)
            {
                return 2;
            }
            else if (countRand < 40)
            {
                return 3;
            }
            else if (countRand < 65)
            {
                return 4;
            }
            else if (countRand < 80)
            {
                return 5;
            }
            else if (countRand < 88)
            {
                return 6;
            }
            else if (countRand < 95)
            {
                return 7;
            }
            else
            {
                return 8;
            }
        }

        static Random minutesRadnom = new Random();

        public static int getMinutesFromMidnight()
        {
            return minutesRadnom.Next(0, 1440);
        }

        static Random dayRadnom = new Random();

        public static int getDayOfWeek()
        {
            return dayRadnom.Next(1, 8);
        }

        static Random familyStatusRadnom = new Random();

        public static string getFamilyStatus()
        {
            int familyPerc =  familyStatusRadnom.Next(0, 100);

            if (familyPerc < 50)
            {
                return "Single";
            }
            else if (familyPerc < 80)
            {
                return "Married";
            }
            else if (familyPerc < 15)
            {
                return "Divorced";
            }
            else
            {
                return "Other";
            }
        }

        static Random yCoordsRandom = new Random();

        public static double getYCoords()
        {
            return yCoordsRandom.Next(34744129, 35048442) / 1000000.0;
        }

        static Random xCoordsRandom = new Random();

        public static double getXCoords()
        {
            return xCoordsRandom.Next(31749354, 32340684) / 1000000.0;
        }
    }
}
